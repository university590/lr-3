#include "Output.hpp"

#include <algorithm>
#include <fstream>

using namespace std;

BEGIN_NAMESPACE(MolecularDynamics)

void gnuplotOutput(std::vector<ParticleState> & states,
								const Configuration & config) {

	double infinity = numeric_limits<double>::max();
	double max_x = -infinity, max_y = -infinity, min_x = infinity, min_y = infinity;
	for (auto particleState : states)
		for (auto particle : particleState.getParticles()) {
			double X = particle.equilibrium; // +particle.offset_x;
			double Y = particle.V; // +particle.offset_y;
			if (X > max_x)
				max_x = X;
			if (X < min_x)
				min_x = X;
			if (Y > max_y)
				max_y = Y;
			if (Y < min_y)
				min_y = Y;
		}

	min_x -= 0.1 * fabs(max_x - min_x);
	max_x += 0.1 * fabs(max_x - min_x);

	if (max_y == min_y) {
		max_y += 1;
		min_y -= 0.2;
	}
	else {
		min_y -= 0.3 * fabs(max_y - min_y);
		max_y += 0.2 * fabs(max_y - min_y);
	}

	double statesPerSec = states.size() / config.time;
	size_t step = statesPerSec / config.fps;
	size_t n = states.size() / step;
	double tttt = double(n) / config.fps;

	ofstream datafile(config.dir + config.dataFileName);

	for (int j = 0; j < n; ++j) {
		datafile << endl;
		for (auto particle : states[j * step].getParticles()) {
			double X = particle.equilibrium;
			double Y = particle.V;

			datafile << states[j * step].getTime() << '\t' <<
				X << '\t' << Y << endl;
		}
		datafile << endl;
	}

	datafile.close();

	ofstream scriptfile(config.dir + config.scriptName);
	scriptfile <<	"set term pop" << endl <<
					"set output " << endl <<
					"stats \'" + config.dataFileName + "\'  u 2:3" << endl <<
					"set xr [" + to_string(min_x) + " : " + to_string(max_x) + "]" << endl <<
					"set yr [" + to_string(min_y) + " : " + to_string(max_y) + "]" << endl <<

					//"do for [i=0 : STATS_blocks-1] { " << endl <<
					//"\tplot \'" + config.dataFileName + "\' index i u 2:3 w p pt 7 ps 0.5 " << 
					//		"title column(1)" << endl <<
					//"\tpause " + to_string(1. / (config.fps)) << endl <<
					//"}" << endl << endl <<

					"set term gif animate delay 4" << endl << 
					"set output \'animation.gif\'" << endl <<
					"do for [i=0 : STATS_blocks-1] { " << endl <<
					"\tplot \'" + config.dataFileName + "\' index i u 2:3 w l " <<
					"title column(1)" << endl <<
					"}";
	scriptfile.close();
}

END_NAMESPACE(MolecularDynamics);