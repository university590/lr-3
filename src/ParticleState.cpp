#include "ParticleState.hpp"
#include "ModelingCore.hpp"

using namespace std;

BEGIN_NAMESPACE(MolecularDynamics)

ParticleState::ParticleState(const vector<Particle> & particles, double time) {
	this->particles = particles;
	this->time = time;
}

void ParticleState::addParticle(const Particle & particle) {
	this->particles.push_back(particle);
}

void ParticleState::setTime(double time) {
	this->time = time;
}

const vector<Particle> ParticleState::getParticles() {
	return particles;
}

double ParticleState::getTime() {
	return time;
}

double getEnerjy(ParticleState state, Configuration conf) {
	double enerjy = 0;

	auto particles = state.getParticles();
	size_t N = particles.size();


	for (int i = 0; i < N; ++i)
		enerjy += conf.mass * particles[i].V * particles[i].V / 2 +
			potentialFPU(particles[i].offset - particles[_I(i - 1, N)].offset,
													conf.alpha, conf.beta);

	return enerjy;
}

END_NAMESPACE(MolecularDynamics);