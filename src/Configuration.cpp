#include "Configuration.hpp"

#include <algorithm>

using namespace std;

BEGIN_NAMESPACE(MolecularDynamics)

void Configuration::check() {
	if (count < 2)
		throw invalid_argument("invalid count");

	if (time <= 0)
		throw invalid_argument("invalid time");
	if (timeStep <= 0 && timeStep >= time)
		throw invalid_argument("invalid timeStep");

	if (!dir.empty() || dir[dir.length() - 1] != '\\')
		dir.append("\\");

	if (dataFileName.empty())
		throw invalid_argument("invalid dataFileName");
	if (scriptName.empty())
		throw invalid_argument("invalid scriptName");

	if (alpha > 1 || alpha < 0)
		throw invalid_argument("invalid alpha");
	if (beta < 0)
		throw invalid_argument("invalid beta");

	if (initialDistance <= 0)
		throw invalid_argument("invalid initialDistance_x");

}

END_NAMESPACE(MolecularDynamics);