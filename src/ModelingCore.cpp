#include "ModelingCore.hpp"

#include <algorithm>
#include <map>
#include <omp.h>
#include <iostream>
#include <functional>

using namespace std;

BEGIN_NAMESPACE(MolecularDynamics)

vector<Particle> generateParticles(Configuration & config) {
	vector<Particle> particles(config.count);
	for (int i = 0; i < particles.size(); ++i) {
		particles[i].equilibrium = i * config.initialDistance;
		particles[i].offset = 0;

		auto it = config.initialSpeeds.find(i);
		if (it != config.initialSpeeds.end())
			particles[i].V = it->second;
		else
			particles[i].V = 0;

		particles[i].A = 0;
	}

	return particles;
}

vector<ParticleState> verle(Configuration & config) {
	double alpha = config.alpha;
	double beta = config.beta;
	size_t N = config.count;
	double dt = config.timeStep;
	double m = config.mass;
	double t;

	vector<Particle> initial = generateParticles(config);
	vector<ParticleState> states;

	states.push_back(ParticleState(initial, 0));

	Particle * m_1 = new Particle[N];
	Particle * m_2 = new Particle[N];
	copy(initial.begin(), initial.end(), m_1);
	copy(initial.begin(), initial.end(), m_2);

	Particle * prev = m_1, * current = m_2;

	function<void(void)> saveFunc;
	if (config.outputEnabled)
		saveFunc = [&states, current, N, &t]() {
			states.push_back(ParticleState(vector<Particle>(current, current + N), t));
		};
	else
		saveFunc = []() {};

	for (t = dt; t < config.time; t += dt, swap(current, prev)) {

		for (int i = 0; i < N; ++i)
			current[i].offset = prev[i].offset + prev[i].V * dt +
											0.5 * prev[i].A * dt * dt;

		for (int i = 0; i < N; ++i) {
			current[i].A = -(1 / m) *
				(-derPotentialFPU(current[_I(i + 1, N)].offset - current[i].offset, alpha, beta) +
					derPotentialFPU(current[i].offset - current[_I(i - 1, N)].offset, alpha, beta));

			current[i].V = prev[i].V + 0.5 * prev[i].A * dt + 0.5 * current[i].A * dt;
		}

		saveFunc();
	}

	if (!config.outputEnabled)
		states.push_back(ParticleState(vector<Particle>(current, current + N), config.time));

	return states;
}

vector<ParticleState> simplexVerle(Configuration & config) {
	double alpha = config.alpha;
	double beta = config.beta;
	size_t N = config.count;
	double dt = config.timeStep;
	double m = config.mass;
	double E = 0.193183327;
	double t;

	vector<double> q1(N), q2(N), a1(N), a2(N), v1(N);

	vector<Particle> initial = generateParticles(config);
	vector<ParticleState> states;

	states.push_back(ParticleState(initial, 0));

	Particle * m_1 = new Particle[N];
	Particle * m_2 = new Particle[N];
	copy(initial.begin(), initial.end(), m_1);
	copy(initial.begin(), initial.end(), m_2);

	Particle * prev = m_1, *current = m_2;

	function<void(void)> saveFunc;
	if (config.outputEnabled)
		saveFunc = [&states, current, N, &t]() {
		states.push_back(ParticleState(vector<Particle>(current, current + N), t));
	};
	else
		saveFunc = []() {};

	for (t = dt; t < config.time; t += dt, swap(current, prev)) {
		
		for (int i = 0; i < N; ++i) 
			q1[i] = prev[i].offset + prev[i].V * E * dt;

		for (int i = 0; i < N; ++i) {
			a1[i] = -(1 / m) *
				(-derPotentialFPU(q1[_I(i + 1, N)] - q1[i], alpha, beta) +
					derPotentialFPU(q1[i] - q1[_I(i - 1, N)], alpha, beta));

			v1[i] = prev[i].V + 0.5 * a1[i] * dt;
			q2[i] = q1[i] + v1[i] * (1 - 2 * E) * dt;
		}

		for (int i = 0; i < N; ++i) {
			a2[i] = -(1 / m) *
				(-derPotentialFPU(q2[_I(i + 1, N)] - q2[i], alpha, beta) +
					derPotentialFPU(q2[i] - q2[_I(i - 1, N)], alpha, beta));

			current[i].V = v1[i] + 0.5 * a2[i] * dt;
			current[i].offset = q2[i] + current[i].V * E * dt;
		}

		saveFunc();
	}

	if (!config.outputEnabled)
		states.push_back(ParticleState(vector<Particle>(current, current + N), config.time));

	return states;
}

std::vector<ParticleState> doModeling(Configuration & config) {
	config.check();

	switch (config.method)
	{
	case Method::Verle:
		return verle(config);
	case Method::SimplexVerle:
		return simplexVerle(config);
	default:
		break;
	}

	return {};
}

double derPotentialFPU(double q, double alpha, double beta) {
	return q + alpha * q * q + beta * q * q * q;
}

double potentialFPU(double q, double alpha, double beta) {
	return q * q / 2 + alpha * q * q * q / 3 + beta * q * q * q * q / 4;
}

END_NAMESPACE(MolecularDynamics)