#include "Particle.hpp"

BEGIN_NAMESPACE(MolecularDynamics)

Particle::Particle(double equilibrium, double offset, double V, double A) {
	this->equilibrium = equilibrium;
	this->offset = offset;
	this->V = V;
	this->A = A;
}

END_NAMESPACE(MolecularDynamics);