#ifndef PARTICLE_STATE
#define PARTICLE_STATE

#include <vector>

#include "Particle.hpp"
#include "Defenitions.hpp"
#include "Configuration.hpp"

BEGIN_NAMESPACE(MolecularDynamics)

class ParticleState {
	std::vector<Particle> particles;
	double time;

public:
	ParticleState() {};
	ParticleState(const std::vector<Particle> & particles, double time);
	void addParticle(const Particle & particle);
	void setTime(double time);
	const std::vector<Particle> getParticles();
	double getTime();
};

double getEnerjy(ParticleState state, Configuration conf);

END_NAMESPACE(MolecularDynamics);

#endif