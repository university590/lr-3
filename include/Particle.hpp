#ifndef PARTICLE
#define PARTICLE

#include "Defenitions.hpp"

BEGIN_NAMESPACE(MolecularDynamics)

struct Particle {
	double equilibrium;
	double offset;
	double V;
	double A;

	Particle() {};
	Particle(double equilibrium, double offset, double V, double A);
};

END_NAMESPACE(MolecularDynamics);

#endif
