#ifndef OUTPUT
#define OUTPUT

#include <string>
#include <vector>

#include "Defenitions.hpp"
#include "ParticleState.hpp"
#include "Configuration.hpp"

BEGIN_NAMESPACE(MolecularDynamics)

void gnuplotOutput(std::vector<ParticleState> & states, 
					const Configuration & config);

END_NAMESPACE(MolecularDynamics);

#endif