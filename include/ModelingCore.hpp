#ifndef MODELING_CORE
#define MODELING_CORE

#include <vector>

#include "Defenitions.hpp"
#include "Particle.hpp"
#include "ParticleState.hpp"
#include "Configuration.hpp"

BEGIN_NAMESPACE(MolecularDynamics)

// Condition of isolation
#define _I(index, N) (N + index) % N

std::vector<Particle> generateParticles(Configuration & config);

std::vector<ParticleState> doModeling(Configuration & config);

// The Fermi-Pasta-Ulam potential
double potentialFPU(double q, double alpha, double beta);
// Derivative of the Fermi-Pasta-Ulam potential
inline double derPotentialFPU(double q, double alpha, double beta);

END_NAMESPACE(MolecularDynamics)

#endif
