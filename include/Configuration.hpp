#ifndef CONFIGURATION
#define CONFIGURATION

#include <string>
#include <map>

#include "Defenitions.hpp"

BEGIN_NAMESPACE(MolecularDynamics) 

enum class Method {Verle, SimplexVerle};

#define DEFAULT_SCRIPTNAME std::string("plotscript.p")
#define DEFAULT_DATAFILENAME std::string("particle-states.dat")
#define DEFAULT_DIR std::string("./")
#define DEFAULT_TIME 3.
#define DEFAULT_TIMESTEP 0.05
#define DEFAULT_ALPHA 0.1
#define DEFAULT_BETA 0.1
#define DEFAULT_DISTANCE_X 0.05
#define DEFAULT_COUNT 30
#define DEFAULT_MASS 0.01;
#define DEFAULT_FPS 25;

struct Configuration {
	double						time				= DEFAULT_TIME;
	double						timeStep			= DEFAULT_TIMESTEP;

	bool						outputEnabled		= true;
	std::string					dataFileName		= DEFAULT_DATAFILENAME;
	std::string					scriptName			= DEFAULT_SCRIPTNAME;
	std::string					dir					= DEFAULT_DIR;
	size_t						fps = DEFAULT_FPS;

	double						alpha				= DEFAULT_ALPHA;
	double						beta				= DEFAULT_BETA;

	Method						method				= Method::Verle;

	double						mass				= DEFAULT_MASS;
	double						initialDistance		= DEFAULT_DISTANCE_X;
	size_t						count				= DEFAULT_COUNT;
	std::map<size_t, double>	initialSpeeds;

	void check();
};

END_NAMESPACE(MolecularDynamics);

#endif
