#include "Particle.hpp"
#include "ParticleState.hpp"
#include "ModelingCore.hpp"
#include "Output.hpp"

#include <string>
#include <iostream>
#include <chrono>
#include <fstream>

using namespace MolecularDynamics;
using namespace std;

void test(ostream & out = cout) {
	Configuration conf;
	conf.timeStep = 0.01;

	conf.alpha = 0;
	conf.beta = 500;
	conf.mass = 1;
	conf.count = 500;

	conf.outputEnabled = false;

	size_t center = conf.count / 2;
	conf.initialSpeeds[center] = 1;	

	out << endl;

	for (int method = (int)Method::Verle; method <= (int)Method::SimplexVerle; ++method) {
		conf.method = (Method)method;
		out << "------------ Using method " <<
			(method == (int)Method::Verle ? "Verle" : "SimplexVerle") <<
			" ------------" << endl;

		for (int T = pow(10, 4); T <= pow(10, 6); T *= 10)
		{
			conf.time = T;

			auto startTime = chrono::system_clock::now();
			auto res = doModeling(conf);
			auto endTime = chrono::system_clock::now();

			auto initialState = res.begin();
			auto finalState = --res.end();
			auto enerjy_1 = getEnerjy(*initialState, conf);
			auto enerjy_2 = getEnerjy(*finalState, conf);
			out << "Enerjy(t = 0) = " << enerjy_1 << endl <<
				"Enerjy(t = " + to_string(conf.time) + ") = " << enerjy_2 << endl <<
				"delta = " << enerjy_2 - enerjy_1 << endl <<
				"time: " << 
					(chrono::duration_cast<chrono::milliseconds>(endTime - startTime)).count() << 
				endl << endl;
		}
	}
}

int main() {

	Configuration conf;
	conf.dir = "./";
	conf.timeStep = 0.01;
	conf.time = 100;
	conf.alpha = 0;
	conf.beta = 1000;
	conf.mass = 1;
	conf.count = 500;
	conf.initialDistance = 0.08;
	conf.method = Method::SimplexVerle;
	conf.outputEnabled = false;
	conf.fps = 7;

	size_t center = conf.count / 2;

	conf.initialSpeeds[center] = 1;
	//conf.initialSpeeds[center - 1] = -1;

	auto startTime = chrono::system_clock::now();
	auto res = doModeling(conf);
	auto endTime = chrono::system_clock::now();

	auto initialState = res.begin();
	auto finalState	= --res.end();
	auto enerjy_1 = getEnerjy(*initialState, conf);
	auto enerjy_2 = getEnerjy(*finalState, conf);

	if (conf.outputEnabled)
		gnuplotOutput(res, conf);

	cout << "Enerjy(t = 0) = " << enerjy_1 << endl <<
		"Enerjy(t = " + to_string(conf.time) + ") = " << enerjy_2 << endl <<
		"delta = " << enerjy_2 - enerjy_1 << endl << endl <<
		"time: " << (chrono::duration_cast<chrono::milliseconds>(endTime - startTime)).count() << endl;

	//ofstream file("enerjy-results.txt");
	//test(file);
	//file.close();

	system("pause");
	return 0;
}